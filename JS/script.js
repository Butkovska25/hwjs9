/* 
1. document.createElement(tag), document.createTextNode(text).
2. Перший параметр - position DOMString - визначає позицію елемента, що додається, щодо елемента, який викликав метод.
beforebegin до  element.
afterbegin одразу після відкривального тега element.
beforeend одразу перед закриваючим тегом element (після останнього нащадка).
afterend після element (після закриваючого тега).
3. Метод Element.remove().



Необов'язкове завдання підвищеної складності:
Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву:
["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.
*/



let parent = document.body;
let arr = ["hello", "world", "Kyiv", "Kharkiv", "Odessa", "Lviv"];

function list(arr, parent) {
    ul = document.createElement('ul');
    parent.append(ul);
    for (let key in arr) {
        li = document.createElement('li');
        ul.append(li);
        li.textContent = arr[key];
    }
};
list(arr, parent)

/*let timerId = setTimeout(() => alert(""), 3000);
alert(timerId);



setTimeout(list, 1000);
*/






